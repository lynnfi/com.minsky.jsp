package com.lm;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.lm.bean.*;
/**
 * Servlet implementation class CheckServlet
 */
public class CheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    	    throws ServletException, IOException {
    	        response.setContentType("text/html;charset=UTF-8");
    	        PrintWriter out = response.getWriter();
    	        String name=request.getParameter("uname");
    	        String pword=request.getParameter("pword");
    	        out.println("<br><br><br><hr><center><font color=red size=12><B>");
    	        try{
    	        UserBean user=new UserBean();
    	        if(user.check(name,pword))
    	            out.println("login success");
    	        else
    	            out.println("login fail");
    	        }catch(Exception e){
    	            out.println("create UserBean failed!");

    	        }
    	        out.println("</B></font></center>");
    	        out.close();
    	    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		response.setCharacterEncoding("UTF-8");
//		response.getWriter().write("菜鸟教程：http://www.runoob.com");
		processRequest(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);		
	}
	 public String getServletInfo() {
	        return "Short description";
	    }
}
